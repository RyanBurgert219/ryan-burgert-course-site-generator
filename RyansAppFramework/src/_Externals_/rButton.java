package _Externals_;//Created by Ryan on 4/18/17.
import javafx.scene.control.Button;
public class rButton extends Button
{
    private Runnable action=null;
    public void setAction(Runnable action)
    {
        setOnAction(e->action.run());
    }
    public Runnable getAction()
    {
        assert action!=null;//You should have used setAction() by now.
        return action;
    }
}
