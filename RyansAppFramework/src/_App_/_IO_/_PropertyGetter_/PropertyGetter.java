package _App_._IO_._PropertyGetter_;//Created by Ryan on 4/10/17.
import _App_.App;
import _Externals_._Resources_.ResourceGetter;
@SuppressWarnings({"WeakerAccess","FieldCanBeLocal"})
public class PropertyGetter
{
    public App app;
    public PropertyGetter(App app)
    {
        this.app=app;
    }
    public String getProperty(String key)//The reason this is private is because every key is accessed via a method.
    {
        return ResourceGetter.getProperty(key);//Actual mechanics of loading file must be outsourced to externals to preserve the logical integrity of my blackbox.
    }
    //region Specific property-getters
    public String getAppTitle()
    {
        return getProperty("app_title");
    }
    public String getAppIconName()
    {
        return getProperty("app_icon_name");
    }
    public double getMinAppHeight()
    {
        return Integer.parseInt(getProperty("app_min_height"));
    }

    //endregion
    //region If I ever want to use XML for some reason
    // private final String XMLDataPath="/Users/Ryan/Desktop/RyanCourseSiteGenerator/TAManager_Solution/data/app_properties.xml";
    // private final String XMLSchemaPath="/Users/Ryan/Desktop/RyanCourseSiteGenerator/TAManager_Solution/data/properties_schema.xsd";
    // //region getProperty(String key)―――――――――――――――――――――――――――――――――――――――――
    // private PropertiesManager pm=PropertiesManager.getPropertiesManager();//Dependent on McKenna's Framework here
    // private void initialize()
    // {
    //     try
    //     {
    //         pm.loadProperties(XMLDataPath,XMLSchemaPath,true);
    //     }
    //     catch(InvalidXMLFileFormatException e)
    //     {
    //         e.printStackTrace();
    //     }
    // }
    //endregion
}
