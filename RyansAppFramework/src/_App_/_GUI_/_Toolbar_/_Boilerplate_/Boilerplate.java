package _App_._GUI_._Toolbar_._Boilerplate_;//Created by Ryan on 4/10/17.
import _App_.App;
import _App_._GUI_._Toolbar_._Actions_.Actions;
import _Externals_.rButton;
@SuppressWarnings("WeakerAccess")
public class Boilerplate extends javafx.scene.control.ToolBar
{
    public App app;
    private final _App_._GUI_._Window_._Boilerplate_.Boilerplate megaplate;
    //
    public Boilerplate(App app,_App_._GUI_._Window_._Boilerplate_.Boilerplate megaplate)//This constructor is holy. Don't change it.
    {
        this.app=app;//Required by Ryan's Framework.
        this.megaplate=megaplate;
        Actions actions=app.gui.toolbar.actions;
        getNewButton().setAction(actions::handleNew);
        getOpenButton().setAction(actions::handleOpen);
        getSaveButton().setAction(actions::handleSave);
        getSaveAsButton().setAction(actions::handleSaveAs);
        getExitButton().setAction(actions::handleExit);
        getExitButton().setAction(actions::handleExit);
        getExitButton().setAction(actions::handleExit);
        getExitButton().setAction(actions::handleExit);
    }
    public rButton getNewButton()
    {
        return megaplate.new_button;
    }
    public rButton getOpenButton()
    {
        return megaplate.open_button0;
    }
    public rButton getSaveButton()
    {
        return megaplate.save_button1;
    }
    public rButton getSaveAsButton()
    {
        return megaplate.saveas_button2;
    }
    public rButton getExportButton()
    {
        return megaplate.export_button3;
    }
    public rButton getExitButton()
    {
        return megaplate.power_button4;
    }
    public rButton getUndoButton()
    {
        return megaplate.undo_button5;
    }
    public rButton getRedoButton()
    {
        return megaplate.redo_button6;
    }
    public rButton getInfoButton()
    {
        return megaplate.book_button7;
    }
}


