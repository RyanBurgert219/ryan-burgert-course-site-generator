package _App_._GUI_._Toolbar_._Actions_;//Created by Ryan on 4/10/17.
import _App_.App;
public class Actions
{
    public App app;
    public Actions(App app)
    {
        this.app=app;
    }
    public void handleNew()
    {

    }
    public void handleOpen()
    {

    }
    public void handleSave()
    {

    }
    public void handleSaveAs()
    {

    }
    public void handleExport()
    {

    }
    public void handleExit()
    {
        //TODO Ask the user to save if save button is enabled
            System.out.println("Exiting");
            app.stage.close();
    }
}
