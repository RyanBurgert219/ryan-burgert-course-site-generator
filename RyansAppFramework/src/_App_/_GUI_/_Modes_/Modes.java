package _App_._GUI_._Modes_;//Created by Ryan on 4/10/17.
import _App_.App;
import _App_._GUI_._Modes_._TAData_.TAData;
import _App_._GUI_._Modes_._ProjectData_.ProjectData;
import _App_._GUI_._Modes_._ScheduleData_.ScheduleData;
import _App_._GUI_._Modes_._CourseDetails_.CourseDetails;
import _App_._GUI_._Modes_._RecitationData_.RecitationData;
@SuppressWarnings("WeakerAccess")
public class Modes
{
    public TAData tadata;
    public ProjectData projectData;
    public ScheduleData scheduleData;
    public CourseDetails courseDetails;
    public RecitationData recitationData;
    public Modes(App app,_App_._GUI_._Window_._Boilerplate_.Boilerplate megaplate)
    {
        tadata=new TAData(app,megaplate);
        projectData=new ProjectData(app,megaplate);
        scheduleData=new ScheduleData(app,megaplate);
        courseDetails=new CourseDetails(app,megaplate);
        recitationData=new RecitationData(app,megaplate);
    }
}